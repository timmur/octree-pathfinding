#include <iostream>
#include <fstream>
#include <yaml-cpp/yaml.h>

int main() {
  // Set start & goal
  std::vector<double> start{0.2, 0.3, 0.2}; //2, 3, 2
  std::vector<double> goal{6.1, 6.1, 6.1}; //61, 61, 61
  // Create a map
  std::vector<double> origin{0, 0, 0}; // set origin at (0, 0, 0)
  std::vector<int> dim(3, 64); 	 // set the number of cells in each dimension as 20, 10, 1
  double res = 0.1; // set resolution as 1m
  std::vector<int> data; // occupancy data, the subscript follows: id = x + dim.x * y + dim.x * dim.y * z;
  data.resize(dim[0] * dim[1] * dim[2], 0); // initialize as free map, free cell has 0 occupancy

  for(int x = 10; x < 60; x ++) {
    for(int y = 25; y < 50; y ++) {
	  for(int z = 30; z < 40; z ++) {
        int id = x + dim[0] *y + dim[0]*dim[1] *z;
        data[id] = 100;
	  }
    }
  }

 for(int x = 20; x < 30; x ++) {
    for(int y = 15; y < 25; y ++) {
	  for(int z = 15; z < 25; z ++) {
      int id = x + dim[0] *y + dim[0]*dim[1] *z;
      data[id] = 100;
	  }
    }
  }
  
  for(int x = 50; x < 55; x ++) {
    for(int y = 55; y < 60; y ++) {
	  for(int z = 5; z < 60; z ++) {
      int id = x + dim[0] *y + dim[0]*dim[1] *z;
      data[id] = 100;
	  }
    }
  }

  for(int x = 0; x < 15; x ++) {
    for(int y = 35; y < 40; y ++) {
	  for(int z = 5; z < 60; z ++) {
      int id = x + dim[0] *y + dim[0]*dim[1] *z;
      data[id] = 100;
	  }
    }
  }
  
  
  


  YAML::Emitter out;
  out << YAML::BeginSeq;
  // Encode start coordinate
  out << YAML::BeginMap;
  out << YAML::Key << "start" << YAML::Value << YAML::Flow << start;
  out << YAML::EndMap;
  // Encode goal coordinate
  out << YAML::BeginMap;
  out << YAML::Key << "goal" << YAML::Value << YAML::Flow << goal;
  out << YAML::EndMap;
  // Encode origin coordinate
  out << YAML::BeginMap;
  out << YAML::Key << "origin" << YAML::Value << YAML::Flow << origin;
  out << YAML::EndMap;
  // Encode dimension as number of cells
  out << YAML::BeginMap;
  out << YAML::Key << "dim" << YAML::Value << YAML::Flow << dim;
  out << YAML::EndMap;
  // Encode resolution
  out << YAML::BeginMap;
  out << YAML::Key << "resolution" << YAML::Value << res;
  out << YAML::EndMap;
  // Encode occupancy
  out << YAML::BeginMap;
  out << YAML::Key << "data" << YAML::Value << YAML::Flow << data;
  out << YAML::EndMap;

  out << YAML::EndSeq;
  std::cout << "Here is the example map:\n" << out.c_str() << std::endl;

  std::ofstream file;
  file.open("simple.yaml");
  file << out.c_str();
  file.close();

  return 0;
}
