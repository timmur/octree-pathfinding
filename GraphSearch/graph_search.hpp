#include "graph_search.h"
#include <cmath>
#include <chrono>

GraphSearch::GraphSearch(const char* cMap, int xDim, int yDim, int zDim, double eps, bool verbose) :
    cMap_(cMap), xDim_(xDim), yDim_(yDim), zDim_(zDim), eps_(eps), verbose_(verbose)
{
	//generate octree here 
	
    //hm_.resize(xDim_ * yDim_ * zDim_);
    //seen_.resize(xDim_ * yDim_ * zDim_, false);

    // Set 3D neighbors
    // for(int x = -1; x <= 1; x ++) {
        // for(int y = -1; y <= 1; y ++) {
            // for(int z = -1; z <= 1; z ++) {
                // if(x == 0 && y == 0 && z == 0) continue;
                // ns_.push_back(std::vector<int>{x, y, z});
            // }
        // }
    // }
    // jn3d_ = std::make_shared<JPS3DNeib>();
}


inline int GraphSearch::coordToId(int x, int y, int z) const {
    return x + y*xDim_ + z*xDim_*yDim_;
}

inline bool GraphSearch::isFree(int x, int y, int z) const {
    return x >= 0 && x < xDim_ && y >= 0 && y < yDim_ && z >= 0 && z < zDim_ &&
        cMap_[coordToId(x, y, z)] == val_free_;
}

inline bool GraphSearch::isOccupied(int x, int y, int z) const {
    return x >= 0 && x < xDim_ && y >= 0 && y < yDim_ && z >= 0 && z < zDim_ &&
        cMap_[coordToId(x, y, z)] > val_free_;
}

inline double GraphSearch::getHeur(int x, int y, int z) const {
    return eps_ * std::sqrt((x - xGoal_) * (x - xGoal_) + (y - yGoal_) * (y - yGoal_) + (z - zGoal_) * (z - zGoal_));
}

bool GraphSearch::plan(int xStart, int yStart, int zStart, int xGoal, int yGoal, int zGoal, bool useJps, int maxExpand)
{
    pq_.clear();
    path_.clear();
    hm_.clear();
    //hm_.resize(xDim_ * yDim_ * zDim_);
    
    auto t1 = std::chrono::steady_clock::now();
    ConstructOctree();
    std::cout << "tree construction time: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - t1).count() << "ms" << std::endl;
  
    seen_.resize(hm_.size(), false);
    
    
    xGoal_ = xGoal; yGoal_ = yGoal; zGoal_ = zGoal;
    xStart_ = xStart; yStart_ = yStart; zStart_ = zStart;
    SetTargets();
    
    
    StatePtr currNode_ptr = hm_[start_id];
    currNode_ptr->g = 0;
    currNode_ptr->h = getHeur(xStart, yStart, zStart);

    return plan(currNode_ptr, maxExpand);
}

bool GraphSearch::plan(StatePtr& currNode_ptr, int maxExpand) {
    // Insert start node
    currNode_ptr->heapkey = pq_.push(currNode_ptr);
    currNode_ptr->opened = true;
    //hm_[currNode_ptr->id] = currNode_ptr;
    seen_[currNode_ptr->id] = true;


    auto t1 = std::chrono::steady_clock::now();
    int expand_iteration = 0;
    while(true)
    {
        expand_iteration++;
        // get element with smallest cost
        currNode_ptr = pq_.top(); pq_.pop();
        currNode_ptr->closed = true; // Add to closed list

        if(currNode_ptr->id == goal_id) {
            //if(verbose_)
                printf("Goal Reached!!!!!!\n\n");
                std::cout << "Astar time: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - t1).count() << "ms" << std::endl;
  
            break;
        }

        //printf("expand: %d, %d\n", currNode_ptr->x, currNode_ptr->y);
        std::vector<int> succ_ids;
        std::vector<double> succ_costs;
        // Get successors
        
        getSucc(currNode_ptr, succ_ids, succ_costs);
        
        // if(verbose_)
        // printf("size of succs: %zu\n", succ_ids.size());
        // Process successors
        for( int s = 0; s < (int) succ_ids.size(); s++ )
        {
            //see if we can improve the value of succstate
            StatePtr& child_ptr = hm_[succ_ids[s]];
            double tentative_gval = currNode_ptr->g + succ_costs[s];

            if( tentative_gval < child_ptr->g )
            {
                child_ptr->parentId = currNode_ptr->id;    // Assign new parent
                child_ptr->g = tentative_gval;        // Update gval

                //double fval = child_ptr->g + child_ptr->h;

                // if currently in OPEN, update
                if( child_ptr->opened && !child_ptr->closed) {
                    pq_.increase( child_ptr->heapkey );             // update heap
                }
                // if currently in CLOSED
                else if( child_ptr->opened && child_ptr->closed)
                {
                    printf("ASTAR ERROR!\n");
                }
                else // new node, add to heap
                {
                    //printf("add to open set: %d, %d\n", child_ptr->x, child_ptr->y);
                    child_ptr->heapkey = pq_.push(child_ptr);
                    child_ptr->opened = true;
                }
            } //
        } // Process successors


        if(maxExpand > 0 && expand_iteration >= maxExpand) {
            if(verbose_)
                printf("MaxExpandStep [%d] Reached!!!!!!\n\n", maxExpand);
            return false;
        }

        if( pq_.empty()) {
            //if(verbose_)
                printf("Priority queue is empty!!!!!!\n\n");
            return false;
        }
    }

    //if(verbose_) {
        printf("goal g: %f, h: %f!\n", currNode_ptr->g, currNode_ptr->h);
        printf("Expand [%d] nodes!\n", expand_iteration);
    //}

    path_ = recoverPath(currNode_ptr);

    return true;
}


std::vector<StatePtr> GraphSearch::recoverPath(StatePtr node) {
    std::vector<StatePtr> path;
    path.push_back(node);
    while(node && node->id != start_id) {
        node = hm_[node->parentId];
        path.push_back(node);
    }

    return path;
}


//to be remade
void GraphSearch::getSucc(const StatePtr& curr, std::vector<int>& succ_ids, std::vector<double>& succ_costs) {
    for(const auto& new_id: curr->neighbors_ids) {
      //int new_x = curr->x + d[0];
      //int new_y = curr->y + d[1];
      //int new_z = curr->z + d[2];
      //if(!isFree(new_x, new_y, new_z))
        //continue;

      //int new_id = coordToId(new_x, new_y, new_z);
      if(!seen_[new_id]) {
        seen_[new_id] = true;
        //hm_[new_id] = std::make_shared<State>(new_id, new_x, new_y, new_z, 0,0,0);
        hm_[new_id]->h = getHeur(hm_[new_id]->x, hm_[new_id]->y, hm_[new_id]->z);
      }
		std::array<float, 3> d;
		d[0] = hm_[new_id]->x - curr->x;
		d[0] = hm_[new_id]->y - curr->y;
		d[0] = hm_[new_id]->z - curr->z;
      succ_ids.push_back(new_id);
      succ_costs.push_back(std::sqrt(d[0]*d[0]+d[1]*d[1]+d[2]*d[2]));
    }
}



std::vector<StatePtr> GraphSearch::getPath() const {
    return path_;
}

std::vector<StatePtr> GraphSearch::getOpenSet() const {
    std::vector<StatePtr> ss;
    for(const auto& it: hm_) {
        if(it && it->opened && !it->closed)
            ss.push_back(it);
    }
    return ss;
}

std::vector<StatePtr> GraphSearch::getCloseSet() const {
    std::vector<StatePtr> ss;
    for(const auto& it: hm_) {
        if(it && it->closed)
            ss.push_back(it);
    }
    return ss;
}


std::vector<StatePtr> GraphSearch::getAllSet() const {
    std::vector<StatePtr> ss;
    for(const auto& it: hm_) {
        if(it)
            ss.push_back(it);
    }
    return ss;
}
