#include "./read_map.hxx"
#include "../GraphSearch/graph_search.hpp"
#include "../GraphSearch/octree.hxx"

int main(int argc, char** argv){
	
	if (argc < 2){
		std::cout << "Give a map filename in parameters!" << std::endl;
		return -1;
	}
	MapReader reader(argv[1]);
	
	if (!reader.exist()){
		std::cout << "Could not parse map!" << std::endl;
		return -2;
	}
	
	std::vector<int> start = reader.CoordToID(reader.start());
	std::vector<int> goal = reader.CoordToID(reader.goal());
	std::vector<int> dim = reader.dim();
	auto origin = reader.origin();
	auto resolution = reader.resolution();
	const std::vector<char> & data = reader.data();
	
	GraphSearch gs(data.data(), dim[0], dim[1], dim[2]);
	
	//gs.PrintInfo();
	std::cout << "start \t goal \t dim" << std::endl;
	for (int i = 0; i < 3; i++)
		std::cout << start[i] << "\t" << goal[i] << "\t" << dim[i] << std::endl;
	
	gs.plan(start[0], start[1], start[2], goal[0], goal[1], goal[2], false, -1);

	auto path = gs.getPath();
	float total_dist = 0.0f;
	for (auto it = path.begin(); it != path.end()-1; it++){
		total_dist += sqrt( pow((float)((*(it+1))->x - (*it)->x), 2) +  pow((float)((*(it+1))->y - (*it)->y), 2) +  pow(((float)(*(it+1))->z - (*it)->z), 2));
	}
	
	std::cout << "path:" << std::endl;
	for (auto it = path.begin(); it != path.end(); it++){
		std::cout << (*it)->x << "\t" << (*it)->y << "\t" << (*it)->z << std::endl;
	}
	
	std::cout << "path distance: " << total_dist << std::endl;
	
	return 0;
}
