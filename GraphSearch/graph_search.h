/**
 * @file graph_search.h
 * @brief backend of graph search, implementation of A* and JPS
 */

#ifndef JPS_GRAPH_SEARCH_H
#define JPS_GRAPH_SEARCH_H

#include <boost/heap/d_ary_heap.hpp>            // boost::heap::d_ary_heap
#include <memory>                                                 // std::shared_ptr
#include <limits>                                                 // std::numeric_limits
#include <vector>                                                 // std::vector
#include <unordered_map>                                    // std::unordered_map
#include <set>
#include <cstdlib>

#define L 0
#define R 1
#define B 0
#define F 2
#define D 0
#define U 4
#define LBD 0
#define RBD 1
#define LFD 2
#define RFD 3
#define LBU 4
#define RBU 5
#define LFU 6
#define RFU 7

//i.e. right-back-up index RBU = 5
#define encodeLabel(xl, yl, zl) xl+yL+zL

//define treenode states
#define UNSET -2
#define UNKNOWN -1
#define WHITE 0
#define BLACK 1
#define GRAY 2
 
    ///Heap element comparison
    template <class T>
    struct compare_state
    {
        bool operator()(T a1, T a2) const
        {
            double f1 = a1->g + a1->h;
            double f2 = a2->g + a2->h;
            if( ( f1 >= f2 - 0.000001) && (f1 <= f2 + 0.000001) )
                return a1->g < a2->g; // if equal compare gvals
            return f1 > f2;
        }
    };


    ///Define priority queue
    struct State; // forward declaration
    struct TreeNode;
    ///State pointer
    using StatePtr = std::shared_ptr<State>;
    using NodePtr = std::shared_ptr<TreeNode>;
    using priorityQueue = boost::heap::d_ary_heap<StatePtr, boost::heap::mutable_<true>,
                                                boost::heap::arity<2>, boost::heap::compare< compare_state<StatePtr> >>;

    ///Node of the graph in graph search
    struct State
    {
        /// ID
        int id;
        /// Coord
        int x, y, z = 0;
        /// direction
        int dx, dy, dz;       		// discrete coordinates of this node
        /// id of predicessors
        int parentId = -1;

        /// pointer to heap location
        priorityQueue::handle_type heapkey;

		std::set<int> neighbors_ids; 
	
        /// g cost
        double g = std::numeric_limits<double>::infinity();
        /// heuristic cost
        double h;
        /// if has been opened
        bool opened = false;
        /// if has been closed
        bool closed = false;

        /// 2D constructor
        State(int id, int x, int y, int dx, int dy )
            : id(id), x(x), y(y), dx(dx), dy(dy)
        {}

        /// 3D constructor
        State(int id, int x, int y, int z, int dx, int dy, int dz )
            : id(id), x(x), y(y), z(z), dx(dx), dy(dy), dz(dz)
        {}

    };
    
    
    struct index{
	int x, y, z = 0;
	index(){}
	index(int x_, int y_, int z_){
		x = x_;
		y = y_;
		z = z_;
	}
};
    
    
	struct TreeNode{
		int id; // unique key id
		index C;
		int size;
		int capacity;
		int occupancy = 0;
		int status = UNSET; 
		int label;
		int depth;
		bool containsStartPt = false;
		bool containsGoalPt = false;
			
		NodePtr Parent = nullptr;
		std::vector<NodePtr> children;
		
		std::vector<NodePtr> neighbors;
		
		//info about obstacles from parent node (or whole map for root)
		// stored in array[8] to be further divided to children
		// as obstacles info is not required after tree construction (see status), 
		// info is properly proceeded to children and current array is cleared after children generation
		std::array<std::vector<index>, 8> obstacles;
		
		//define "brethren" face neighbors
		void ConnectChildren();
		int GetStatus();
		void ExternalSearch(std::vector<NodePtr> & nodes);
		bool assessNeighboring(NodePtr);
		
		TreeNode(int _id, int _size, int _Cx, int _Cy, int _Cz, NodePtr _Parent = nullptr, int _label = -1)
		{	
			id = _id;
			size = _size;
			C = index(_Cx, _Cy, _Cz);
			//C.x = _Cx; C.y = _Cy; C.z = _Cz;
			Parent = _Parent;
			label = _label;
		}
	};


	index decodeLabel(int label){
		int x = 0, y = 0, z = 0;
		int temp = label;
		
		if(temp >= 4) {
			temp -= 4;
			z = U;
		}
		
		if (temp >= 2){
			temp -= 2;
			y = F;
		}
		
		x = temp; // L or R
		
		return index(x, y, z);
	}
		

	/**
	* @brief GraphSearch class
	*
	* Implement A*
	*/
	class GraphSearch
	{
	public:
		/**
		* @brief 3D graph search constructor
		*
		* @param cMap 1D array stores the occupancy, with the order equal to \f$x + xDim * y + xDim * yDim * z\f$
		* @param xDim map length
		* @param yDim map width
		* @param zDim map height
		* @param eps weight of heuristic, optional, default as 1
		* @param verbose flag for printing debug info, optional, default as False
		*/
		GraphSearch(const char* cMap, int xDim, int yDim, int zDim, double eps = 1, bool verbose = false);    
		
		NodePtr MakeRootNode(int & maxdepth);
		NodePtr MakeChildNode(NodePtr p, int _label, int _id);
		StatePtr ConvertToGraphNode(NodePtr t);
		void ConstructOctree();
		void SetTargets();

		/**
		* @brief start 3D planning thread
		*
		* @param xStart start x coordinate
		* @param yStart start y coordinate
		* @param zStart start z coordinate
		* @param xGoal goal x coordinate
		* @param yGoal goal y coordinate
		* @param zGoal goal z coordinate
		* @param useJps if true, enable JPS search; else the planner is implementing A*
		* @param maxExpand maximum number of expansion allowed, optional, default is -1, means no limitation
		*/
		bool plan(int xStart, int yStart, int zStart, int xGoal, int yGoal, int zGoal, bool useJps, int maxExpand = -1);
	
		/// Get the optimal path
		std::vector<StatePtr> getPath() const;

		/// Get the states in open set
		std::vector<StatePtr> getOpenSet() const;

		/// Get the states in close set
		std::vector<StatePtr> getCloseSet() const;

		/// Get the states in hash map
		std::vector<StatePtr> getAllSet() const;

        private:
		/// Main planning loop
		bool plan(StatePtr& currNode_ptr, int max_expand);
		/// Get successor function for A*
		void getSucc(const StatePtr& curr, std::vector<int>& succ_ids, std::vector<double>& succ_costs);
		/// Recover the optimal path
		std::vector<StatePtr> recoverPath(StatePtr node);

		/// Get subscript
		int coordToId(int x, int y, int z) const;

		/// Check if (x, y, z) is free
		bool isFree(int x, int y, int z) const;

		/// Check if (x, y, z) is occupied
		bool isOccupied(int x, int y, int z) const;

		/// Calculate heuristic
		double getHeur(int x, int y, int z) const;

	    //yaml map 
		const char* cMap_;
            
	    //map size
	    int xDim_, yDim_, zDim_;
            
	    // =1 by def
	    double eps_;
            
	    bool verbose_;

	    //ocuppancy value
            const char val_free_ = 0;
	    
	    //destination point
		int xGoal_, yGoal_, zGoal_, goal_id;
		int xStart_, yStart_, zStart_, start_id;

	    //frontier
		priorityQueue pq_;
		
	    //navvolumedata?
		std::vector<StatePtr> hm_;
		std::vector<NodePtr> nodes;
	    
		//closed(?)
		std::vector<bool> seen_;

	    //raw path
		std::vector<StatePtr> path_;

	    //a star neighbors vector
		//std::vector<std::vector<int>> ns_;
		// ns is a template for uniform grid 

	};
	
#endif
