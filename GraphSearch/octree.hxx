#include "graph_search.h"

		void TreeNode::ConnectChildren(){
			if (children.empty()) { std::cout <<"no children to connect!" <<std::endl; return;}
			else{//templated neighbors under cut
				
				for (int i = 0; i < 8; i++){
					children[i]->neighbors.clear();
				}
				
				this->children[LBD]->neighbors.push_back(children[RBD]);
				this->children[LBD]->neighbors.push_back(children[LFD]);
				this->children[LBD]->neighbors.push_back(children[LBU]);
				
				this->children[RBD]->neighbors.push_back(children[LBD]);
				this->children[RBD]->neighbors.push_back(children[RFD]);
				this->children[RBD]->neighbors.push_back(children[RBU]);
				
				this->children[LFD]->neighbors.push_back(children[RFD]);
				this->children[LFD]->neighbors.push_back(children[LBD]);
				this->children[LFD]->neighbors.push_back(children[LFU]);
				
				this->children[RFD]->neighbors.push_back(children[LFD]);
				this->children[RFD]->neighbors.push_back(children[RBD]);
				this->children[RFD]->neighbors.push_back(children[RFU]);
				
				this->children[LBU]->neighbors.push_back(children[RBU]);
				this->children[LBU]->neighbors.push_back(children[LFU]);
				this->children[LBU]->neighbors.push_back(children[LBD]);
				
				this->children[RBU]->neighbors.push_back(children[LBU]);
				this->children[RBU]->neighbors.push_back(children[RFU]);
				this->children[RBU]->neighbors.push_back(children[RBD]);
				
				this->children[LFU]->neighbors.push_back(children[RFU]);
				this->children[LFU]->neighbors.push_back(children[LBU]);
				this->children[LFU]->neighbors.push_back(children[LFD]);
				
				this->children[RFU]->neighbors.push_back(children[LFU]);
				this->children[RFU]->neighbors.push_back(children[RBU]);
				this->children[RFU]->neighbors.push_back(children[RFD]);
			}
		}
		

		int TreeNode::GetStatus(){
			//if (status == UNSET) DefineStatus();
			return status;
		}
		
		
		void TreeNode::ExternalSearch(std::vector<NodePtr> & nodes){
			for (auto it = Parent->neighbors.begin(); it != Parent->neighbors.end(); it++){
			//for (auto & it : Parent->neighbors){
				bool areNeighbors = assessNeighboring(*it);
				//std::cout << this->id << " + " << (*it)->id << " = " <<  (int) areNeighbors << std::endl;
				if (areNeighbors){
					this->neighbors.push_back(*it);
					(*it)->neighbors.push_back(nodes[this->id]);
				}
			}
		}
		
		bool TreeNode::assessNeighboring(NodePtr n){
			int ox = -5, oy = -5, oz = -5; // overlap result for each axis
			
			if(this->C.x + this->size == n->C.x ||
				n->C.x + n->size == this->C.x) ox = 1;
			else if(this->C.x + this->size > n->C.x &&
				n->C.x + n->size > this->C.x) ox = 2;
				
			if(this->C.y + this->size == n->C.y ||
				n->C.y + n->size == this->C.y) oy = 1;
			else if(this->C.y + this->size > n->C.y &&
				n->C.y + n->size > this->C.y) oy = 2;
				
			if(this->C.z + this->size == n->C.z ||
				n->C.z + n->size == this->C.z) oz = 1;
			else if(this->C.z + this->size > n->C.z &&
				n->C.z + n->size > this->C.z) oz = 2;
			
			return (ox + oy + oz == 5);
		}




		NodePtr GraphSearch::MakeRootNode(int & maxdepth){
			//define tree size: get closest ceiled pow2 for cMap max dimension	
			int maxDim = 0;
			if (xDim_ > yDim_) maxDim = xDim_;
			else maxDim = yDim_;
			if (maxDim < zDim_) maxDim = zDim_;
			
			int treeside = 2;
			maxdepth = 1;
			while(treeside < maxDim) {
				treeside *= 2;
				maxdepth++;
			}
			
			
			NodePtr root = std::make_shared<TreeNode>( 0,	treeside,	0, 0, 0,	nullptr,  -1);
			root->capacity = pow(treeside, 3);//size*size*size;
			
			root->status = UNSET;
			
			root->depth = 0;
			
			root->occupancy = 0;
			
			int xl = L, yl = B, zl = D;//(0,0,0) by def
			for (int x = 0; x < xDim_; x++)
			for (int y = 0; y < yDim_; y++)
			for	(int z = 0; z < zDim_; z++)
				if (isOccupied(x, y, z)){
					xl = x >= treeside/2 ? R:L;
					yl = y >= treeside/2 ? F:B;
					zl = z >= treeside/2 ? U:D; 
					root->occupancy++;
					root->obstacles[xl+yl+zl].push_back(index(x, y, z));
		
				}
			
	
				if (root->occupancy == 0){
					if (root->C.x >= xDim_ || root->C.y >= yDim_ || root->C.z >= zDim_)
						 root->status = UNKNOWN; //node out of map range --- unknown 
					else root->status = WHITE;     //node is free to move through
				}
				else if (root->occupancy == root->capacity) root->status = BLACK; //node is completely blocked, no need to divide further
				else root->status = GRAY; //node contains obstacles, need to be further divided, 
		
	
			return root;
		}
			
		NodePtr GraphSearch::MakeChildNode(NodePtr p, int _label, int _id){
			
			int _size = p->size/2;
			
			index tmp = decodeLabel(_label);
			index C(p->C.x, p->C.y, p->C.z);
			if (tmp.x == R) C.x += _size;
			if (tmp.y == F) C.y += _size;
			if (tmp.z == U) C.z += _size;
			
			NodePtr child = std::make_shared<TreeNode>(_id, _size, C.x, C.y, C.z, p, _label);			
			
			child->capacity = _size*_size*_size;
			child->occupancy = p->obstacles[_label].size();
			child->depth = p->depth + 1;
			
			child->status = UNSET;
				if (child->occupancy == 0){
					if (child->C.x >= xDim_ || child->C.y >= yDim_ || child->C.z >= zDim_)
						 child->status = UNKNOWN; //node out of map range --- unknown 
					else child->status = WHITE;     //node is free to move through
				}
				else if (child->occupancy == child->capacity) child->status = BLACK; //node is completely blocked, no need to divide further
				else child->status = GRAY; //node contains obstacles, need to be further divided, 
			
			if (child->occupancy > 0){
				int xl, yl, zl;
					
				for(auto it = p->obstacles[_label].begin(); it != p->obstacles[_label].end(); ){
					
					xl = (*it).x >= C.x + _size/2 ? R : L;
					yl = (*it).y >= C.y + _size/2 ? F : B;
					zl = (*it).z >= C.z + _size/2 ? U : D;
					child->obstacles[xl+yl+zl].push_back(*it);
					p->obstacles[_label].erase(it);
				}
			}
			if (child->GetStatus() == BLACK)
			//std::cout << "child info: id(" << _id << "); parent(" << p->id << "); depthlvl(" << child.depth << "); status(" << child.status << "); label(" << _label << "); capacity(" << child.capacity << "); occupancy(" << child.occupancy <<"); neighbors - " << child.neighbors.size() << std::endl;

			
			return child;
		}
		
		StatePtr GraphSearch::ConvertToGraphNode(NodePtr t){
			StatePtr s = std::make_shared<State>(t->id,
												 t->C.x + t->size/2,
												 t->C.y + t->size/2, 
												 t->C.z + t->size/2,
												 0, 0, 0);
												 
			//s->neighbors_ids.resize(t->neighbors.size());
			
			for (const auto it : t->neighbors)
				if (nodes[it->id]->GetStatus() == WHITE)	s->neighbors_ids.insert(it->id);
				
			return s;
		}

		void GraphSearch::ConstructOctree(){		
			
			this->nodes.clear();
			
			int maxdepth;
			nodes.push_back(MakeRootNode(maxdepth));
			
			int id = 1;
			//for(auto it = nodes.begin(); it != nodes.end(); it++)
			for (int it = 0; it < nodes.size(); it++){
			
				if (nodes[it]->GetStatus() == GRAY && nodes[it]->depth  < maxdepth){
					
					//create 8 children
					for (int i = 0; i < 8; i++){
						nodes.push_back(MakeChildNode(nodes[it], i, id)); 
						nodes[it]->children.push_back(nodes[id]);
						//nodes[it].neighbors.push_back(&nodes[id]);
						id++;
					}
					     
					//internal search
					nodes[it]->ConnectChildren();
					//external search
					for (int i = 0; i < 8; i++)
						nodes[it]->children[i]->ExternalSearch(nodes);
					
				}
				
			}
			//int test_var_occ = 0;
			//int test_var_cap = 0;
			//int large = 0;
			//int small = 0;
			//for (int it = 0; it < nodes.size(); it++){
				//if (nodes[it]->GetStatus() == WHITE) {
					//test_var_cap += nodes[it]->capacity;
					//if(nodes[it]->depth == maxdepth) small++; else large++;
				//}
				//else if (nodes[it]->GetStatus() == BLACK){
					//test_var_occ += nodes[it]->occupancy;
				//}
				//else if (nodes[it]->GetStatus() == GRAY && nodes[it]->children.empty()) {
					//test_var_occ += nodes[it]->occupancy;
					//test_var_cap += nodes[it]->capacity - nodes[it]->occupancy;
				//}
			//}
			
			//std::cout << test_var_cap << std::endl << test_var_occ << std::endl << small << std::endl << large << std::endl;

			//hm_.resize(large + small);
			for (auto it = nodes.begin(); it != nodes.end(); it++){
				//if ((*it)->GetStatus() == WHITE) 
					hm_.push_back(ConvertToGraphNode(*it));
			}
			
		}

		void GraphSearch::SetTargets(){
			start_id = 0;
			goal_id = 0;
			NodePtr s = nodes[0];
			while(s->GetStatus() != WHITE){
				int xl, yl, zl;
				xl = xStart_ >= s->C.x + s->size/2 ? R : L;  
				yl = yStart_ >= s->C.y + s->size/2 ? F : B;  
				zl = zStart_ >= s->C.z + s->size/2 ? U : D; 
				s = s->children[xl+yl+zl];
				start_id = s->id;
			}
			
			hm_[start_id]->x = xStart_;
			hm_[start_id]->y = yStart_;
			hm_[start_id]->z = zStart_;
			
			std::cout << "start @ depth " << s->depth << "; parent: " << s->Parent->id << "neighbors: " << s->neighbors.size() << std::endl;
			std::cout << "start treenode located @ " << start_id << "; coords: " << hm_[start_id]->x <<", " << hm_[start_id]->y << ", " << hm_[start_id]->z <<std::endl;
			NodePtr g = nodes[0];
			while(g->GetStatus() != WHITE){
				int xl, yl, zl;
				xl = xGoal_ >= g->C.x + g->size/2 ? R : L;  
				yl = yGoal_ >= g->C.y + g->size/2 ? F : B;  
				zl = zGoal_ >= g->C.z + g->size/2 ? U : D; 
				g = g->children[xl+yl+zl];
				goal_id = g->id;
			}
			
			hm_[goal_id]->x = xGoal_;
			hm_[goal_id]->y = yGoal_;
			hm_[goal_id]->z = zGoal_;
			std::cout << "goal @ depth " << g->depth << "; parent: " << g->Parent->id << "neighbors: " << g->neighbors.size() << std::endl;
			
			std::cout << "goal treenode located @ " << goal_id << "; coords: " << hm_[goal_id]->x <<", " << hm_[goal_id]->y << ", " << hm_[goal_id]->z <<std::endl;
			
		}
		
